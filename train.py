# Import libraries
from PIL import Image
import pytesseract
from pdf2image import convert_from_path
import os
import tabula
import camelot


# Adding custom options
custom_config = r'--oem 3 --psm 6'
'''
Part #1 : Converting PDF to images
'''


def skip(text):
    # clean the text
    skips = [".", ":", ";", "'", '"', "(", ")", '\n']
    for ch in skips:
        text = text.replace(ch, " ")
    return text


def extractetables(PDF_file, listofwords):
    # Store all the pages of the PDF in a variable
    pages = convert_from_path(PDF_file, 500)

    # Counter to store images of each page of PDF to image
    image_counter = 1

    listofpages = []

    # listofwords = ["hazardous"]

    # Iterate through all the pages stored above

    for page in pages:
        for word in listofwords:
            if str(word) in skip(str(pytesseract.image_to_string(page, config=custom_config))):
                #         # Increment the counter to update filename

                listofpages.append(image_counter)
                image_counter = image_counter + 1

            # print(listofpages)

        for pagenumber in listofpages:
            # read PDF file
            tables = tabula.io.read_pdf(PDF_file, pages=pagenumber)

            # save them in a folder
            folder_name = "tables"
            if not os.path.isdir(folder_name):
                os.mkdir(folder_name)
            # iterate over extracted tables and export as excel individually
            for i, table in enumerate(tables, start=1):
                table.to_excel(os.path.join(folder_name, f"table_{i}.xlsx"), index=False)
